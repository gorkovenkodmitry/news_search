<?php
include 'application/forms/form_search.php';

class Controller_Main extends Controller {
    function __construct() {
    	$this->view = new View();
        $this->form = new Form_Search();
    }

    function action_index() {
        $data = array (
            'form' => $this->form
        );
        $this->view->generate('main_view.php', 'template_view.php', $data);
    }
}