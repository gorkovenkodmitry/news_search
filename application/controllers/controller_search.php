<?php
include "application/core/sphinxapi.php";
include "application/models/model_news.php";

class Controller_Search extends Controller
{
    function __construct() {
        $this->view = new View();
        $this->form = new Form_Search();
        $this->model = new Model_News();
        $this->requestAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    function action_index() {
        if ($this->requestAjax) {
            $this->form->set_data($_GET, $_SERVER);
            $data = array();
            if ($this->form->is_valid()) {
                $data['status'] = 'success';

                $sphinx = new SphinxClient();
                $sphinx->SetServer('localhost', 9312);

                $sphinx->SetMatchMode(SPH_MATCH_ANY);
                $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
                $w = array ('name' => 20, 'anons' => 10, 'txt' => 5);
                $sphinx->SetFieldWeights($w);
                $sphinx->SetLimits(0, 50);

                $result = $sphinx->Query($this->form->fields['q']['value'], 'news');
                $data['total_found'] = $result['total_found'];
                $data['q'] = $this->form->fields['q']['value'];

                if ($result && $result['total_found'] > 0 && is_array($result['matches'])) {
                    $ids = array_keys($result['matches']);
                    $id_list = implode(',', $ids);
                    $res = $this->model->get_news_list($id_list);
                    $news_list = array();
                    while ($row = $res->fetch_assoc()) {
                        $news_list[$row['id']] = $row;
                    }
                    $data['num_rows'] = $res->num_rows;
                    $data['ids'] = $ids;
                    $data['news_list'] = $news_list;
                }
            } else {
                $data['status'] = 'error';
                $data['errors'] = $this->form->errors;
            }

            
            $this->view->ajax_request($data);
        } else {
            Route::ErrorPage404();
        }
    }
}