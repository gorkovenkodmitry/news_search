<?php

class DataBase {
    private $db_name='portal';
    private $db_user='root';
    private $db_host='localhost';
    private $db_password='';
    private $db;

    function __construct()
    {
        $this->db = new mysqli($this->db_host, $this->db_user, $this->db_password, $this->db_name);
        $this->db->set_charset("UTF8");
        if (mysqli_connect_errno())
        {
            printf("Подключение к серверу MySQL невозможно. Код ошибки: %s\n", mysqli_connect_error());
        }
    }

    function __destruct()
    {
        $this->db->close();
    }

    function get_db()
    {
        return $this->db;
    }
}