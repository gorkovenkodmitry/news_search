<?php
class View {
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function generate($content_view, $template_view, $data = null) {
        include 'application/view/'.$template_view;
    }

    function ajax_request($data = null) {
    	header('Content-Type: application/json');
    	echo json_encode($data);
    }
}