<?php
class Form_Search {
    public $fields = array(
        'q' => array(
            'label' => "Найти", 
            'value' => null,
            'required' => true
        ), 
    );
    public $errors;

    function set_data($data, $server) {
        foreach ($this->fields as $key => $field) {
            if (isset($data[$key])) {
                $this->fields[$key]['value'] = $data[$key];
            }
        }
    }

    function is_valid() {
        $valid = true;
        $this->errors = array();
        foreach ($this->fields as $key => $field) {
            if ($field['required'] && strlen($field['value'])==0) {
                $this->errors[$key] = 'Обязательное поле';
                $valid = false;
            }
        }
        return $valid;
    }

}