<?php
class Model_News extends Model
{
    private $table_name = '_news_cut_';
    private $database;

    function __construct()
    {
        $this->database =  new DataBase();
    }

    public function get_news_list($ids)
    {
        $query = 'SELECT * FROM '.$this->table_name.' WHERE id IN ('.$ids.')';
        $db = $this->database->get_db();
        return $db->query($query);
    }
}