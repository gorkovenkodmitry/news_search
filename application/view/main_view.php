<h1>Сервис поиска новостей</h1>

<div id="search_form">
    <?php
    include 'application/view/search_form.php';
    ?>
</div>

<hr>

<div id="news-list" style="display: none;">
    <h3>Результаты запроса: <i></i></h3>
    <p class="total_found">Всего найдено: <i></i></p>
    <p>На странице ограничение в 50 первых новостей</p>
    <ul class = "list-group"></ul>
</div>