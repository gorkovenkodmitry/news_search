<form action="/search/" method="get" id="search-form">
	<?php foreach ($data['form']->fields as $key => $field): ?>
		<div class="form-group form-group-<?= $key; ?>">
			<label for="id_<?= $key; ?>"><?= $field['label']; ?>:</label>
			<input autocomplete="off" class="form-control" type="text" name="<?= $key; ?>" id="id_<?= $key; ?>" value="<?= $field['value']; ?>" />
			<?php if (isset($data['form']->q_errors)): ?>
				<p class="text-danger"><?php echo $data['form']->q_errors;?></p>
			<?php else: ?>
				<p class="text-danger" style="display: none;"></p>
			<?php endif ?>
		</div>
	<?php endforeach ?>

	<button type="submit" class="btn btn-default">Найти</button>
</form>
