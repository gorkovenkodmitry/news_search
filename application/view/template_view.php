<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <link rel="stylesheet" href="/media/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/media/bootstrap/css/bootstrap-theme.min.css"/>
</head>
<body>
    <div class="container">
        <?php
            include 'application/view/'.$content_view;
        ?>
    </div>
<script type="text/javascript" src="/media/js/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="/media/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/media/js/script.js"></script>
</body>
</html>