function get_obj(data) {
    var obj;
    if (typeof(data) != 'object') {
        obj = jQuery.parseJSON(data);
    } else {
        obj = data;
    }
    if (obj.debug) {
        console.log(obj.debug);
    }
    return obj;
}

var onReady = {
    init: function() {
        $(document).on('click', '#search-form .btn[type="submit"]', onReady.btnSearchClick);
    },

    btnSearchClick: function(e) {
        e.preventDefault();
        var btn = $(this);
        var form = $('#search-form');
        $.ajax({
            url: form.attr('action'),
            method: 'GET',
            data: form.serialize(),
            success: function(data) {
                var obj = get_obj(data);
                var news_list = $('#news-list');
                news_list.show();
                form.find('.form-group').removeClass('has-error');
                form.find('p.text-danger').hide();
                if (obj.status == 'success') {
                    news_list.find('h3 i').text(obj.q);
                    if (obj.total_found == 0) {
                        news_list.find('.list-group').html('<li class="list-group-item">Ничего не найдено</li>');
                        news_list.find('.total_found i').text(0);
                        return true;
                    }
                    news_list.find('.list-group').html('');
                    news_list.find('.total_found i').text(obj.total_found);
                    for (var i=0; i<obj.ids.length; i++) {
                        var article = obj.news_list[obj.ids[i]];
                        var article_html = '<span class = "badge">'+article.news_date+' '+article.news_time+'</span>';
                        article_html += '<div class="article-title">'+article.name+'</div>';
                        article_html += '<div class="article-anons"><small><i>'+article.anons+'</i></small></div>';
                        article_html += '<button data-toggle="collapse" data-target="#article'+article.id+'">Полный текст</button>';
                        article_html += '<div id="article'+article.id+'" class="article-txt collapse">'+article.txt+'</div>';
                        news_list.find('.list-group').append('<li class="list-group-item">'+article_html+'</li>');
                    };
                } else {
                    for (key in obj.errors) {
                        form.find('.form-group-'+key).addClass('has-error').find('p.text-danger').show().text(obj.errors[key]);
                    }
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}

$(document).ready(onReady.init);